import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Text } from "react-native";
import _ from "lodash";
import HeaderComponent from "../components/Header";
import ActiveJobs from "../components/ActiveJobs";
import * as apiRoutes from "../apiroutes"
import axios from 'axios';

export default class HistoryScreen extends Component {
  state = {
    loading: false,
    errors: false
  };
  render() {
    const { navigation } = this.props;
    const endPointUrlActiveJob = apiRoutes.host + apiRoutes.completedJobs
    console.log("endPointUrlActiveJob : ", endPointUrlActiveJob)
    return (
      <View style={{ flex: 1 }}>
        <HeaderComponent title={"Completed Orders"} />
        {this.state.loading && !this.state.errors ? (
          <SpinnerOverlay />
        ) : (
            <View style={styles.container}>
              <ActiveJobs {...this.props} endPointUrl={endPointUrlActiveJob} title="Completed jobs" />
            </View>
          )}
      </View>
    );
  }
}

HistoryScreen.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 15,
    backgroundColor: "#fff"
  }
});
