import React, { Component } from "react";
import {
  StyleSheet,
  View,
  // Button,
  Text,
  Image,
  ActivityIndicator,
  TextInput,
  Picker,
  AsyncStorage,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import * as Animatable from "react-native-animatable";
import { Item, Input, Button } from "native-base";
import { showAlertMessage } from "../../utils/helpers";
import logo from "../../assets/images/logo.png";
import Colors from "../../constants/Colors";
import loginScreenBg from '../../assets/images/loginScreenBg.png';
import FlashMessage, { hideMessage } from "react-native-flash-message";
import * as apiRoutes from "../../apiroutes";
import axios from "axios";

export default class Login extends Component {
  static navigationOptions = {
    drawerLabel: () => null
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      company: "",
      pickerItems: [],
      errorStatus: false,
      errorMsg: "",
      isLoading: false,
      loggedIn: false
    };
    this.handleLogin = this.handleLogin.bind(this);
  }
  componentDidMount() {
    console.log("componentDidMount login");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount login");
    hideMessage();
    this.willUnmount = true;
  }
  handleLogin() {
    if (!this.state.username || !this.state.password) {
      showAlertMessage("error");
      showAlertMessage("error", "Error, Some of required fields are missing!");
      this.setState({
        errorStatus: true,
        errorMsg: "Some of required fields are missing..."
      });
    } else {
      this.setState({ isLoading: true }, () => {
        let loginCred = {
          user_name: this.state.username,
          password: this.state.password
        };
        this.startSignInAndSetStorage(loginCred);
      });
    }
  }
  startSignInAndSetStorage(loginCred) {
    console.log("#loginCred : ", loginCred)
    try {
      const login = apiRoutes.host + apiRoutes.login
      console.log("#login : ", login)
      axios
        .post(login, loginCred)
        .then((response) => {
          // console.log("#response.data : ", response.data)
          axios.defaults.baseURL = apiRoutes.host;
          axios.defaults.headers.common["Authorization"] =
            "Bearer " + response.data.token;
          AsyncStorage.setItem("isLoggedIn", "true")
          AsyncStorage.setItem("host", apiRoutes.host)
          AsyncStorage.setItem("token", JSON.stringify(response.data.token))
          this.setState(
            {
              isLoading: false
            },
            () => {
              this._redirect()
            }
          );
        }).catch((error) => {
          console.log("#error : ", error)
          this.setState(
            {
              isLoading: false
            },
            showAlertMessage(
              "error",
              "Error, Unable to validate Credationals!",
              error.message
            )
          );
        })
    } catch (error) {
      console.log("#error : ", error)
      console.log("TRY LOG IN ==>", error);
      this.setState({
        errorStatus: true,
        errorMsg: error.message,
        isLoading: false
      });
    }
  }
  // Fetch the token from storage then navigate to our appropriate place
  _redirect = () => {
    const { navigation } = this.props;
    const token = AsyncStorage.getItem("token");
    const isLoggedIn = AsyncStorage.getItem("isLoggedIn");
    // const host = await AsyncStorage.getItem("host");
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    // console.log("token authLoad: ", token);
    console.log("isLoggedIn loginScreen: ", isLoggedIn);
    token.then((token) => {
      // console.log("token : ", JSON.parse(token))
      if (token && isLoggedIn) {
        // console.log("token : ", JSON.parse(token))
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + JSON.parse(token);
        navigation.navigate("Main");
      }
      else {
        this.props.navigation.navigate("Login");
      }
    })
    // this.props.navigation.navigate("Main");
  };
  render() {
    return (
      // <ImageBackground
      //   source={loginScreenBg}
      //   style={[ {width: "100%", height: "auto" }]}
      //   imageStyle={{ resizeMode: 'contain' }}>
      <View style={styles.container}>
        {/* <Image source={loginScreenBg} style={{width:"auto",height:"auto"}}/> */}
        <Animatable.View
          animation="bounceInDown"
          easing="ease-in-quad"
          delay={2}
        // iterationCount="infinite"
        >
          <Image source={logo} style={{ width: 200, height: 150 }} />

          {/* <Text style={{ fontSize: 24, paddingTop: 10, paddingBottom: 10 }}>Karachi Home Foods</Text> */}
        </Animatable.View>
        <Animatable.View
          animation="zoomIn"
          easing="ease-in-circ"
          delay={2}
        // iterationCount="infinite"
        >
          <Text style={{ fontSize: 24, paddingTop: 10, paddingBottom: 30 }}>Chef Application</Text>
        </Animatable.View>
        <Item rounded style={styles.inputField}>
          <Input placeholder='User Name'
            placeholder="User Name"
            autoFocus
            onChangeText={e => {
              this.setState({ username: e });
            }} />
        </Item>
        <Item rounded style={styles.inputField}>
          <Input
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={e => {
              this.setState({ password: e });
            }} />
        </Item>
        {/* <TextInput
          style={styles.inputField}
          placeholder="User Name"
          autoFocus
          onChangeText={e => {
            this.setState({ username: e });
          }}
        /> */}
        {/* <TextInput
          style={styles.inputField}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={e => {
            this.setState({ password: e });
          }}
        /> */}
        {this.state.isLoading ? (
          <ActivityIndicator size="large" color={Colors.themeLightColor}/>
        ) : (
            <TouchableOpacity onPress={this.handleLogin}>
              <Button rounded style={{ backgroundColor: Colors.themeDarkColor, width: 280, marginTop: 10, justifyContent: "center", textAlign: "center" }}>
                <Text style={{ color: "#ffff", fontWeight: "bold" }}>Login</Text>
              </Button>
            </TouchableOpacity>
          )}
        {/* <View style={{ width: 300, marginTop: 10, justifyContent: "center", textAlign: "center" }}>
          <Button color="#2fa4e7" title="SignIn" onPress={this.handleLogin} />
        </View> */}
        <FlashMessage position="top" animated={true} />
      </View>
      // </ImageBackground>
    );
  }
}
Login.navigationOptions = {
  header: null
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF"
  },
  welcome: {
    // marginLeft: 30,
    // marginBottom: 40,
    // marginRight: 30
  },
  inputField: {
    // color: "#333333",
    // padding: 5,
    // borderBottomWidth: 1,
    // borderColor: "#4682B4",
    marginBottom: 10,
    width: 280,
    height: 40
  },
  pickerStyle: {
    borderBottomWidth: 1,
    width: 280,
    backgroundColor: "#ecf4fc",
    marginTop: 5,
    marginBottom: 10
  },
  activityIndStyle: {
    margin: 20
  }
});
