import React from "react";
import { AsyncStorage, StatusBar, View } from "react-native";
import Spinner from "../../components/Spinner";
import axios from "axios";
export default class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    console.log("componentDidMount AuthLoadingScreen");
    this._redirect();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _redirect = () => {
    const token = AsyncStorage.getItem("token");
    const isLoggedIn = AsyncStorage.getItem("isLoggedIn");
    // const host = await AsyncStorage.getItem("host");
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    // console.log("token authLoad: ", token);
    // console.log("isLoggedIn authLoad: ", isLoggedIn);
    token.then((token)=> {
      // console.log("token : ", JSON.parse(token))
      if (token && isLoggedIn) {
        // console.log("token : ", JSON.parse(token))
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + JSON.parse(token);
        this.props.navigation.navigate("Main");
      }
      else {
        this.props.navigation.navigate("Login");
      }
    })
    // this.props.navigation.navigate("Main");
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Spinner />
        {/* <StatusBar barStyle="default" /> */}
      </View>
    );
  }
}
