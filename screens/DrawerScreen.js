import React, { Component } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet,
  AsyncStorage,
  Button
} from "react-native";
import { Divider } from "react-native-elements";
// import { DrawerItems, NavigationActions } from "react-navigation";
import { ListItem } from "react-native-elements";
import { menuItems } from "../navigation/drawerList";
import logo from "../assets/images/logo.png";
// import { DrawerItems } from "react-navigation";

export default class DrawerScreen extends Component {
  companyId = "";
  _signOutAsync = () => {
    const clearStorage = AsyncStorage.clear();
    clearStorage.then(() => {
      this.props.navigation.navigate("Login");
    }).catch(() => alert("Unable to logout !"))
  };
  navigateToScreen = item => () => {
    if (item.label == "Logout") {
      this._signOutAsync()
    }
    else if (item.screen == "HomeScreen") {
      this.props.navigation.closeDrawer();
    } else {
      this.props.navigation.navigate(item.screen, {
        title: item.label,
        ...this.props
      });
    }
  };

  render() {
    AsyncStorage.getItem("session").then(resp => { });
    return (
      <SafeAreaView
        style={styles.SafeAreaFlex}
        forceInset={{ top: "always", horizontal: "never" }}
      >
        <View style={styles.ViewStyling}>
        <Image source={logo} style={{ width: 200, height: 150 }} />
        </View>
        <Divider />
        <ScrollView>
          {/*<DrawerItems {...this.props}/> */}
          <View>
            {menuItems.length > 0
              ? menuItems.map((item, i) => (
                <ListItem
                  key={i}
                  title={item.label}
                  titleStyle={{
                    color: item.screen == "HomeScreen" ? "blue" : item.label == "Logout" ? "red" : "black"
                  }}
                  leftIcon={{ name: item.icon || null, type: item.type ? item.type : "" }}
                  bottomDivider
                  chevron
                  onPress={this.navigateToScreen(item)}
                />
              ))
              : ""}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  SafeAreaFlex: {
    flex: 1
  },
  ViewStyling: {
    marginTop: 35,
    // height: 60,
    backgroundColor: "#ffff",
    alignItems: "center",
    justifyContent: "center"
  }
});
