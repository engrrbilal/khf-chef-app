import React, { Component } from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage
} from "react-native";
import HeaderComponent from "../components/Header";
import ActiveJobs from "../components/ActiveJobs";
import * as apiRoutes from "../apiroutes"
import axios from 'axios';

export default class HomeScreen extends Component {
  static navigationOptions = {
    header: null
  };
  componentDidMount() {
    console.log("componentDidMount HomeScreen");
    // this._redirect();
  }

  // Fetch the token from storage then navigate to our appropriate place
  // _redirect = () => {
  //   const token = AsyncStorage.getItem("token");
  //   const isLoggedIn = AsyncStorage.getItem("isLoggedIn");
  //   // const host = await AsyncStorage.getItem("host");
  //   // This will switch to the App screen or Auth screen and this loading
  //   // screen will be unmounted and thrown away.
  //   // console.log("token authLoad: ", token);
  //   // console.log("isLoggedIn authLoad: ", isLoggedIn);
  //   token.then((token)=> {
  //     // console.log("token : ", JSON.parse(token))
  //     if (token && isLoggedIn) {
  //       // console.log("token : ", JSON.parse(token))
  //       axios.defaults.headers.common["Authorization"] =
  //         "Bearer " + JSON.parse(token);
  //     }
  //     else {
  //       this.props.navigation.navigate("Login");
  //     }
  //   })
  //   // this.props.navigation.navigate("Main");
  // };
  render() {
    const endPointUrlActiveJob = apiRoutes.host + apiRoutes.activeJobs
    console.log("endPointUrlActiveJob : ",endPointUrlActiveJob)
    return (
      <View style={styles.container}>
        <HeaderComponent
          title="Dashboard"
          leftIcon="menu"
          rightIcon="home"
          navigation={this.props.navigation}
        />
        <View style={styles.container}>
          <ActiveJobs {...this.props} endPointUrl={endPointUrlActiveJob} title="Active jobs"/>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  welcomeContainer: {
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    // marginTop: 20
  }
});
