import React from 'react'
import {
  StyleSheet, Text, View
} from 'react-native'
import { Body, Card, CardItem, List, ListItem, Icon } from 'native-base';
import HeaderComponent from '../../components/Header';

export default class ProfileScreen extends React.Component {


  render() {
    return (

      <View style={{ flex: 1 }}>
        <HeaderComponent
          title="Profile"
          leftIcon="keyboard-arrow-left"
          navigation={this.props.navigation}
        />
        <List>
          <ListItem itemHeader first style={{ justifyContent: 'space-between',flexDirection:"row" }}>
            <View>
              <Text style={{ fontWeight: 'bold', fontSize: 18 }}>SH # SH-101</Text>
            </View>
            <View style={{ flexDirection:"row",justifyContent:"space-around" }}>
              <Text style={{ paddingTop: 4,paddingRight:5 }}>9.3</Text>
              <Icon name='star' style={{ fontSize: 25, color: "orange"}} />
            </View>
          </ListItem>
          <ListItem>
            <Text onPress={() => this.props.navigation.navigate('ProfileInfo')}>Profile Info</Text>
          </ListItem>
          <ListItem onPress={() => this.props.navigation.navigate('PaymentInfo')}>
            <Text>Payment Info</Text>
          </ListItem>
        </List>
      </View>

    )

  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});




