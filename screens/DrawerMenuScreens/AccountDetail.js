import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Text } from "react-native";
import HeaderComponent from "../../components/Header";
import { Body, Card, CardItem, List, ListItem } from 'native-base';
import axios from 'axios';
import * as Animatable from "react-native-animatable";
import * as apiRoutes from "../../apiroutes";
import Colors from "../../constants/Colors";
export default class AccountDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReady: false,
            chosenDate: new Date(),
            chosenDate2: new Date(),
            accountData: {}

        };
    }    
    // componentDidMount() {
    //     const endPointUrlAccountData = apiRoutes.host + apiRoutes.accountDetail
    //     console.log("endPointUrlAccountData : ", endPointUrlAccountData)
    //     axios
    //         .get(endPointUrlAccountData)
    //         .then(response => {
    //             console.log("#response.data acounts: ", response.data)
    //             this.setState({
    //                 accountData: response.data
    //             })
    //         }).catch(error => console.log("error activejob: ", error)
    //         )
    // }
    render() {
        const { params } = this.props.navigation.state;
        const account = params.account
        const total = Number(account["earning"]) + Number(account["bonus"]) - Number(account["disputed"]) - Number(account["fine"])
        // Object= {
        //   "bonus": "300",
        //   "disputed": "0",
        //   "earning": "2500",
        //   "end_date": "2020-02-20",
        //   "fine": "0",
        //   "id": 5,
        //   "payment_status": "Remaining",
        //   "received": 0,
        //   "remaining": 2500,
        //   "start_date": "2020-02-13",
        //   "week": "Old",
        // }
        return (
            <View style={{ flex: 1 }}>
                <HeaderComponent
                    title="Account Detail"
                    leftIcon="keyboard-arrow-left"
                    navigation={this.props.navigation}
                />
                <ScrollView style={styles.container}>
                    {/* <Content> */}
                    <List>
                        {/* <ListItem style={{ justifyContent: 'space-between' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{this.state.chosenDate.toDateString()}</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>To</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{this.state.chosenDate2.toDateString()}</Text>
                        </ListItem> */}
                        <ListItem style={{ justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Eaning :</Text>
                            <Animatable.View
                                animation="pulse"
                                // easing="ease-in-circ"
                                delay={2}
                                iterationCount={4}
                            >
                                <Text style={{ fontSize: 16 }}>Rs : {account["earning"]}</Text>
                            </Animatable.View>
                        </ListItem>
                        <ListItem style={{ justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Bonus :</Text>
                            <Text style={{ fontSize: 16 }}>Rs : {account["bonus"]}</Text>
                        </ListItem>
                        <ListItem style={{ justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Disputed :</Text>
                            <Text style={{ fontSize: 16 }}>Rs : {account["disputed"]}</Text>
                        </ListItem>
                        <ListItem style={{ justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Fine :</Text>
                            <Text style={{ fontSize: 16 }}>Rs : {account["fine"]}</Text>
                        </ListItem>
                    </List>
                    <View>
                        <Animatable.View
                            animation="fadeInDown"
                            // easing="ease-in-circ"
                            delay={2}
                        // iterationCount={4}
                        >
                            <Card style={{ borderRadius: 5 }}>
                                <CardItem bordered style={{ backgroundColor: Colors.themeLightColor }}>
                                    <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                                            Total :
                               </Text>
                                        <Animatable.View
                                            animation="pulse"
                                            // easing="ease-in-circ"
                                            delay={2}
                                            iterationCount={4}
                                        >
                                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                                                {total}
                                            </Text>
                                        </Animatable.View>
                                    </Body>
                                </CardItem>

                            </Card>
                        </Animatable.View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

AccountDetailScreen.navigationOptions = {
    header: null
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: "#fff"
    },
    borderBoxStyle: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#d6d7da",
        width: "45%",
        height: 40,
        justifyContent: "center", alignItems: "center",
        marginBottom: 15, marginTop: 15
    },
});