import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
} from 'react-native';
import { Body, Card, CardItem, List, ListItem, Container, Content } from 'native-base';
import axios from 'axios';
import HeaderComponent from '../components/Header';
import * as apiRoutes from "../apiroutes"
import { showAlertMessage } from '../utils/helpers';
import * as Animatable from "react-native-animatable";
import { colors } from 'react-native-elements';
import Colors from '../constants/Colors';
export default class JobScreen extends Component {
    state = {
        isLoading: false
    }
    componentDidMount() {
        console.log("componentDidMount JobScreen")
        this.willFocusSubscription = this.props.navigation.addListener(
            "willFocus",
            () => {
                console.log("componentDidMount willFocus JobScreen")
            }
        );
    }

    upadeStatusNotifierWithOrderId = () => {
        try {
            this.setState({ isLoading: true }, () => {
                const { params } = this.props.navigation.state;
                const activeJob = params.activeJob
                const statusNotifierUrl = apiRoutes.host + apiRoutes.statusNotifierWithOrderId + activeJob.id
                console.log("#statusNotifierUrl : ", statusNotifierUrl)
                axios
                    .put(statusNotifierUrl)
                    .then((response) => {
                        console.log("#response.data : ", response.data)
                        this.setState(
                            {
                                isLoading: false
                            }, () => alert(response.data.msg))

                    }).catch((error) => {
                        console.log("#error : ", error)
                    })
            })
        } catch (error) {
            console.log("#error : ", error)
            this.setState({
                errorStatus: true,
                errorMsg: error.message,
                isLoading: false
            });
        }
    }
    render() {
        // console.log("sss",this.props.navigation)
        const { params } = this.props.navigation.state;
        const activeJob = params.activeJob
        // console.log("#activeJob : ", activeJob)
        // console.log("#activeJob.status : ", activeJob.status)

        let today = new Date(activeJob.pickup_time);
        let dd = today.getDate();

        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }
        today = mm + '-' + dd + '-' + yyyy;

        let pickupTime = new Date(activeJob.pickup_time)
        let pickupHours = pickupTime.getHours();
        let pickupMinutes = pickupTime.getMinutes();
        pickupTime = pickupHours + ':' + pickupMinutes;

        return (
            <View style={styles.container}>
                <Container>
                    <HeaderComponent
                        title={"Order Detail"}
                        leftIcon="arrow-back"
                        navigation={this.props.navigation}
                    />
                    <Text style={styles.tabBarInfoText}>
                        Menu
                        </Text>
                    <Content padder>
                        <View style={{ margin: 30, marginTop: 10, marginBottom: 10 }}>
                            <Card style={{ borderRadius: 5 }}>
                                <Animatable.View
                                    animation="bounceInDown"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                >
                                    <CardItem style={{ backgroundColor: Colors.themeDarkColor, borderRadius: 0 }}>
                                        <Body style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                                                Name
                                        </Text>
                                            <Text style={{ fontWeight: 'bold', color: "#ffff", fontSize: 20 }} >
                                                Quantity
                                 </Text>
                                        </Body>
                                    </CardItem>

                                </Animatable.View>
                                {activeJob.order_items.length ? activeJob.order_items.map((orderItem, index) => {
                                    return (
                                        <CardItem key={index} style={{ justifyContent: 'space-between', backgroundColor: Colors.themeLightColor, borderRadius: 0 }}>
                                            {/* <Animatable.View
                                                animation="fadeInLeft"
                                                easing="ease-in-quad"
                                                delay={2}
                                            // iterationCount="infinite"
                                            > */}
                                            <Text style={{ fontSize: 16,color:"#ffff" }}>{orderItem.menu_item.name} </Text>
                                            {/* </Animatable.View> */}
                                            <Animatable.View
                                                animation="fadeInRight"
                                                easing="ease-in-quad"
                                                delay={2}
                                            // iterationCount="infinite"
                                            >
                                                <Text style={{ fontSize: 16,color:"#ffff" }}>{orderItem.quantity}</Text>
                                            </Animatable.View>
                                        </CardItem>
                                    )

                                }) : undefined}
                                {console.log("activeJob.id : ", activeJob.id)}
                                {activeJob.special_instructions ?
                                    // <Animatable.View
                                    //     animation="fadeInLeft"
                                    //     easing="ease-in-circ"
                                    //     delay={2}
                                    // // iterationCount="infinite"
                                    // >
                                    <CardItem header style={{ justifyContent: "flex-start", alignItems: "flex-start", flexDirection: "column" }}>
                                        <Text style={{ fontSize: 16, fontWeight: "bold" }}>Special Instructions </Text>
                                        <Text style={{ fontSize: 14 }}>{activeJob.special_instructions}</Text>
                                    </CardItem>
                                    // </Animatable.View>
                                    : undefined}

                            </Card>
                        </View>
                        <List>
                            <ListItem style={{ justifyContent: 'space-between' }}>
                                {/* <Animatable.View
                                    animation="fadeInLeft"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                > */}
                                <Text style={{ fontSize: 16 }}>Pickup Time :</Text>
                                {/* </Animatable.View> */}
                                <Animatable.View
                                    animation="fadeInRight"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                >
                                    <Text style={{ fontSize: 16 }}>{pickupTime}</Text>
                                </Animatable.View>
                            </ListItem>
                            <ListItem style={{ justifyContent: 'space-between' }}>
                                {/* <Animatable.View
                                    animation="fadeInLeft"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                > */}
                                <Text style={{ fontSize: 16 }}>Rider Name :</Text>

                                {/* </Animatable.View> */}
                                <Animatable.View
                                    animation="fadeInRight"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                >
                                    <Text style={{ fontSize: 16 }}>{activeJob.rider.first_name} {activeJob.rider.last_name}</Text>
                                </Animatable.View>
                            </ListItem>
                            <ListItem style={{ justifyContent: 'space-between' }}>
                                {/* <Animatable.View
                                    animation="fadeInLeft"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                > */}
                                <Text style={{ fontSize: 16 }}>Date :</Text>
                                {/* </Animatable.View> */}
                                <Animatable.View
                                    animation="fadeInRight"
                                    easing="ease-in-quad"
                                    delay={2}
                                // iterationCount="infinite"
                                >
                                    <Text style={{ fontSize: 16 }}>{today}</Text>
                                </Animatable.View>
                            </ListItem>
                            <ListItem style={{ justifyContent: 'space-between' }}>
                                <Animatable.View
                                    animation="pulse"
                                    // easing="ease-in-circ"
                                    delay={2}
                                    iterationCount={4}
                                >
                                    <Text style={{ fontSize: 16 }}>Your Earning :</Text>
                                </Animatable.View>
                                <Animatable.View
                                    animation="pulse"
                                    // easing="ease-in"
                                    delay={2}
                                    iterationCount={4}
                                >
                                    <Text style={{ fontSize: 16 }}>{activeJob.chef_earning}</Text>
                                </Animatable.View>
                            </ListItem>
                        </List>
                    </Content>
                    {activeJob.status != "Delivered" ?
                        <View style={{ flexDirection: "row", justifyContent: "space-around", paddingTop: 5, paddingBottom: 5, paddingLeft: 15, paddingRight: 15 }}>
                            <View style={{ width: 300 }}>
                                <Button title={activeJob.status} disabled={this.state.isLoading} onPress={this.upadeStatusNotifierWithOrderId} style={{ borderRadius: 10, backgroundColor: '#329cd1' }} />
                            </View>
                        </View> : undefined
                    }
                </Container>
                {/* <LocationAccess/> */}
            </View>
        );
    }
}

JobScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tabBarInfoText: {
        // marginTop: 40,
        fontSize: 20,
        fontWeight: "bold",
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
});
