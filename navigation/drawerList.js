import AccountScreen from "../screens/DrawerMenuScreens/AccountScreen";
import ProfileScreen from "../screens/DrawerMenuScreens/ProfileScreen";
import SupportScreen from "../screens/DrawerMenuScreens/supportscreen";
import AccountDetailScreen from "../screens/DrawerMenuScreens/AccountDetail";
import PaymentInfo from "../screens/DrawerMenuScreens/PaymentInfo";
import ProfileInfo from "../screens/DrawerMenuScreens/ProfileInfo";
import SettingsScreen from "../screens/SettingsScreen";


export const menuItems = [
  {
    type: "command",
    screen: "HomeScreen",
    label: "Orders",
    icon: "home"
  },
  {
    type: "command",
    screen: "AccountScreen",
    label: "Account",
    icon: "file",
    type: "font-awesome",
    column: 2
  },
  // {
  //   type: "command",
  //   screen: "SupportScreen",
  //   label: "Support",
  //   column: 2
  // },
  {
    type: "command",
    screen: "ProfileScreen",
    label: "Profile",
    icon: "user",
    type: "font-awesome",
    column: 2
  },
  {
    type: "command",
    screen: "Logout",
    label: "Logout",
    icon:'ios-log-out',
    type:'ionicon',
    column: 2
  }
];

export default {
  AccountScreen: {
    screen: AccountScreen
  },
  AccountDetail: {
    screen: AccountDetailScreen
  },
  ProfileScreen: {
    screen: ProfileScreen
  },
  SupportScreen: {
    screen: SupportScreen
  },
  PaymentInfo: {
    screen: PaymentInfo
  },
  ProfileInfo: {
    screen: ProfileInfo
  },
  SettingsScreen: {
    screen: SettingsScreen
  }
};
