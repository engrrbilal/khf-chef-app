import React from "react";
import { Platform, PixelRatio, Image } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import SettingsScreen from "../screens/SettingsScreen";
import JobScreen from "../screens/JobScreen";
import HistoryScreen from "../screens/HistoryScreen";
// import GoogleMapScreen from "../screens/GoogleMapScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    JobScreen: JobScreen,
    // GoogleMap: GoogleMapScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: "Active Jobs",
  // tabBarIcon: ({ focused }) => (
  //   <TabBarIcon
  //     focused={focused}
  //     name={
  //       Platform.OS === "ios"
  //         ? `home${focused ? "" : "-outline"}`
  //         : "md-home"
  //     }
  //   />
  // )
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        source={require('../assets/images/active-orders.png')}
        style={{
          width:25,
          height:25
        }}
      />
    )
  }
};

HomeStack.path = "";

const HistoryStack = createStackNavigator(
  {
    History: HistoryScreen,
    JobScreen: JobScreen,
  },
  config
);

HistoryStack.navigationOptions = {
  tabBarLabel: "Orders History",
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        source={require('../assets/images/complete-orders.png')}
        style={{
          width:25,
          height:25,
          margin:15
        }}
     />
    )
  }
  // tabBarIcon: ({ focused }) => (
  //   <TabBarIcon
  //     focused={focused}
  //     name={
  //       Platform.OS === "ios"
  //         ? `ios-done-all${focused ? "" : "-outline"}`
  //         : "md-done-all"
  //     }
  //   />
  // )
};
HistoryStack.path = "";
const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: "Settings",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-options" : "md-options"}
    />
  )
};

SettingsStack.path = "";

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  HistoryStack,
  // SettingsStack
});

tabNavigator.path = "";

export default tabNavigator;
