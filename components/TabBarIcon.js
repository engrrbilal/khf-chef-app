import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import * as Animatable from "react-native-animatable";

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  return (
    <Animatable.View
      animation="zoomInUp"
      // easing="ease-in-circ"
      delay={2}
    // iterationCount="infinite"
    >
      <Ionicons
        name={props.name}
        size={26}
        style={{ marginBottom: -3 }}
        color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    </Animatable.View>
  );
}
