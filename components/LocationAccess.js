import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet, Button } from 'react-native';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

export default class LocationAccess extends Component {
    state = {
        location: null,
        errorMessage: null,
    };

    componentDidMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
            this._getLocationAsync();
        }
    }

    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }

        let location = await Location.getCurrentPositionAsync({});
        console.log("location: ",JSON.stringify(location))
        const locationParams = {latitude:location.coords["latitude"],longitude:location.coords["longitude"]}
        console.log("locationParams: ",JSON.stringify(locationParams))
        Location.reverseGeocodeAsync(location)
        this.setState({ location });
    }
    addressToLocation = async () => {
        const address = "Model Colony,kazimabad malir"
        const locationFind = Location.geocodeAsync(address)
        console.log("locationFind: ",JSON.stringify(locationFind))
        const addressReturn = Location.reverseGeocodeAsync(locationFind)
        console.log("addressReturn: ",JSON.stringify(addressReturn))

    }

    render() {
        let text = 'Waiting..';
        if (this.state.errorMessage) {
            text = this.state.errorMessage;
        } else if (this.state.location) {
            text = JSON.stringify(this.state.location);
        }

        return (
            <View style={styles.container}>
                <Text style={styles.paragraph}>Rider Location : {text}</Text>
                {/* <Button title="Address location" onPress={this.addressToLocation} /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#ecf0f1',
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        textAlign: 'center',
    },
});