
export const host = "http://ec2-3-0-57-66.ap-southeast-1.compute.amazonaws.com:3000";
export const login = "/auth/chef/login";
export const activeJobs = "/chef/order/active";
export const profileData = "/chef/profile";
export const completedJobs = "/chef/order/completed";
export const statusNotifierWithOrderId = "/chef/order/";
export const accountDetail = "/chef/account/details";

// export const accounts = "/chef/account/details";
export const accountsBalanceDetail = "/chef/account/balance/details";
